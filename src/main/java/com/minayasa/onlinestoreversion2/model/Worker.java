package com.minayasa.onlinestoreversion2.model;

public class Worker {
    private int id;
    private String name;
    private double tunjanganPulsa;
    private double gajiPokok;
    private int absen = 20;

    public String getWorkerType() {
        return workerType;
    }

    public void setWorkerType(String workerType) {
        this.workerType = workerType;
    }

    private String workerType;

    public Worker() {
    }

    public Worker(int id, String name, double tunjanganPulsa, double gajiPokok, int absen, String workerType) {
        this(name, tunjanganPulsa,gajiPokok, absen);
        this.workerType = workerType;
        this.id = id;
    }

    public Worker(String name, double tunjanganPulsa, double gajiPokok, int absen) {
        this.name = name;
        this.tunjanganPulsa = tunjanganPulsa;
        this.gajiPokok = gajiPokok;
        this.absen = absen;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getTunjanganPulsa() {
        return tunjanganPulsa;
    }

    public void setTunjanganPulsa(double tunjanganPulsa) {
        this.tunjanganPulsa = tunjanganPulsa;
    }

    public double getGajiPokok() {
        return gajiPokok;
    }

    public void setGajiPokok(double gajiPokok) {
        this.gajiPokok = gajiPokok;
    }

    public int getAbsen() {
        return absen;
    }

    public void setAbsen(int absen) {
        this.absen = absen;
    }

    public void addAbsen() {
        absen++;
    }

}
