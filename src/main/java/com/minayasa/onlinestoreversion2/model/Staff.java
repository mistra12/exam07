package com.minayasa.onlinestoreversion2.model;

public class Staff extends Worker {

    private double tunjanganMakan;

    public Staff() {}

    public Staff(String name, double tunjanganPulsa, double gajiPokok, int absen) {
        super(name, tunjanganPulsa, gajiPokok, absen);
    }

    public double hitunTunjanganMakan() {
        tunjanganMakan = getAbsen() * 20000;
        return tunjanganMakan;
    }

}