package com.minayasa.onlinestoreversion2.model;

public class Manager extends Worker {

    int tunjanganTransport;
    int tunjanganEntertaint;

    public Manager(int tunjanganTransport, int tunjanganEntertaint) {
        this.tunjanganTransport = tunjanganTransport;
        this.tunjanganEntertaint = tunjanganEntertaint;
    }

    public Manager( String name, double tunjanganPulsa, double gajiPokok, int absen, int tunjanganTransport, int tunjanganEntertaint) {
        super( name, tunjanganPulsa, gajiPokok, absen);
        this.tunjanganTransport = tunjanganTransport;
        this.tunjanganEntertaint = tunjanganEntertaint;
    }

    public int hitungTunjanganTransport() {
        tunjanganTransport = getAbsen() * 50000;
        return tunjanganTransport;
    }

    public void hitungTunjanganEntertaint(int jumlahEntertaint) {
        tunjanganEntertaint = jumlahEntertaint * 500000;
    }


}
