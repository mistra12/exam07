package com.minayasa.onlinestoreversion2.controller;

import com.minayasa.onlinestoreversion2.model.Worker;
import com.minayasa.onlinestoreversion2.service.WorkerService;
import com.minayasa.onlinestoreversion2.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class RestApiController {

    public static final Logger logger = LoggerFactory.getLogger(RestApiController.class);

    @Autowired
    WorkerService workerService;


    // -------------------Laporan Gaji--------------------------------------------

    @RequestMapping(value = "/workers", method = RequestMethod.GET)
    public ResponseEntity<?> listAllWorkes()  {
        List<Worker> workers = workerService.findAllWorkers();
        if (workers.isEmpty()) {
            return new ResponseEntity<>(workers, HttpStatus.NOT_FOUND);
        }
        int totalGaji = 0;
        for (Worker worker: workers) {
            totalGaji += (worker.getAbsen() * worker.getGajiPokok() + worker.getTunjanganPulsa());
        }

        Map<String, Object> response = new HashMap<>();
        response.put("totalGaji", totalGaji);
        response.put("workers", workers);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    // -------------------Retrieve Single Worker------------------------------------------

    @RequestMapping(value = "/worker/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getWorker(@PathVariable("id") int id)  {
        logger.info("Fetching Product with id {}", id);
        Worker worker = workerService.findById(id);
        if (worker == null) {
            logger.error("Worker with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Worker with id " + id  + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(worker, HttpStatus.OK);
    }

    // -------------------Create a Worker-------------------------------------------

    @RequestMapping(value = "/worker/", method = RequestMethod.POST)
    public ResponseEntity<?> createProduct(@RequestBody Worker worker)  {
        logger.info("Creating Product : {}", worker);

        if (workerService.isWorkerExist(worker)) {
            logger.error("Unable to create. A Product with name {} already exist", worker.getName());
            return new ResponseEntity<>(new CustomErrorType("Unable to create. A Product with name " +
                    worker.getName() + " already exist."), HttpStatus.CONFLICT);
        }
        workerService.saveWorker(worker);

        return new ResponseEntity<>(worker, HttpStatus.CREATED);
    }

    // ------------------- Update a Worker ------------------------------------------------

    @RequestMapping(value = "/worker/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateProduct(@PathVariable("id") int id, @RequestBody Worker worker)  {
        logger.info("Updating Worker with id {}", id);

        Worker currentWorker = workerService.findById(id);

        if (currentWorker == null) {
            logger.error("Unable to update. Worker with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to upate. Worker with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }

        currentWorker.setName(worker.getName());
        currentWorker.setWorkerType(worker.getWorkerType());
        currentWorker.setGajiPokok(worker.getGajiPokok());
        currentWorker.setTunjanganPulsa(worker.getTunjanganPulsa());

        workerService.updateWorker(currentWorker);
        return new ResponseEntity<>(currentWorker, HttpStatus.OK);
    }

    // ------------------- Absen Worker-----------------------------------------

    @RequestMapping(value = "/worker/absen/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> absenWorker(@PathVariable("id") int id, @RequestBody Worker worker)  {
        logger.info("Absens Worker with id {}", id);

        Worker workerAbsen = workerService.findById(id);

        if (workerAbsen == null) {
            logger.error("Unable to absen Worker with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to absen. Worker with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }

        workerAbsen.addAbsen();

        workerService.addAbsen(workerAbsen);

        return new ResponseEntity<>(workerAbsen, HttpStatus.OK);
    }

    // ------------------- Hitung Tunjangan Manager-----------------------------------------

    @RequestMapping(value = "/worker/tunjangan-manager/{id}/{lamaEntertain}", method = RequestMethod.GET)
    public ResponseEntity<?> hitungTunjanganManger(@PathVariable("id") int id, @PathVariable("lamaEntertain") int lamaEntertaint) {
        Worker worker = workerService.findById(id);

        if (worker.getWorkerType().equalsIgnoreCase("staff")) {
            return new ResponseEntity<>(new CustomErrorType("Worker with id " + id + " is staff."),
                    HttpStatus.NOT_FOUND);
        }

        Map<String, Object> response = new HashMap<>();
        int tunjanganTranport = worker.getAbsen() * 50000;
        int tunjanganEntertain = lamaEntertaint * 500000;

        response.put("name", worker.getName());
        response.put("Total tunjangan transport", tunjanganTranport);
        response.put("Total tunjangan entertaint", tunjanganEntertain);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    // ------------------- Hitung Tunjangan Staff-----------------------------------------

    @RequestMapping(value = "/worker/tunjangan-staff/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> hitungTunjanganStaff(@PathVariable("id") int id) {
        Worker staff = workerService.findById(id);

        if (staff.getWorkerType().equalsIgnoreCase("manager")) {
            return new ResponseEntity<>(new CustomErrorType("Worker with id " + id + " is manager."),
                    HttpStatus.NOT_FOUND);
        }

        Map<String, Double> response = new HashMap<>();
        double tunjanganMakan = staff.getAbsen() * 20000.0;


        response.put("tunjanganMakan",  tunjanganMakan);
        response.put("tunjanganPulsa",  staff.getTunjanganPulsa());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    // ------------------- Hitung Total Gaji-----------------------------------------

    @RequestMapping(value = "/workers/total-gaji", method = RequestMethod.GET)
    public ResponseEntity<?> totalGaji()  {
        List<Worker> workers = workerService.findAllWorkers();
        if (workers.isEmpty()) {
            return new ResponseEntity<>(workers, HttpStatus.NOT_FOUND);
        }

        Map<String, Double> response = new HashMap<>();
        for (Worker worker : workers) {
            response.put(worker.getName(), ( (worker.getAbsen() * worker.getGajiPokok()) + (worker.getAbsen() * 20000) + worker.getTunjanganPulsa()));
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }



    // ------------------- Delete a Product-----------------------------------------

    @RequestMapping(value = "/product/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProduct(@PathVariable("id") int id)  {
        logger.info("Fetching & Deleting Product with id {}", id);

        Worker worker = workerService.findById(id);
        if (worker == null) {
            logger.error("Unable to delete. Worker with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to delete. Worker with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        workerService.deleteWorkerById(id);
        return new ResponseEntity<Worker>(HttpStatus.NO_CONTENT);
    }


}

