package com.minayasa.onlinestoreversion2.service;

import java.util.List;
import com.minayasa.onlinestoreversion2.model.Worker;

public interface WorkerService {

    Worker findById(int id);

    Worker findByName(String name);

    void saveWorker(Worker worker);

    void updateWorker(Worker worker);

    void addAbsen(Worker worker);

    void deleteWorkerById(int id);

    List<Worker> findAllWorkers();

    void deleteAllProducts();

    boolean isWorkerExist(Worker worker);

}
