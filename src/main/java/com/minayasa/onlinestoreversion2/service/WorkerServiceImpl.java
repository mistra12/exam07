package com.minayasa.onlinestoreversion2.service;

import com.minayasa.onlinestoreversion2.model.Product;
import com.minayasa.onlinestoreversion2.model.Worker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service("workerService")
public class WorkerServiceImpl implements WorkerService {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    //  Using two hashmaps in order to provide performance of O(1) while fetching products
    private static HashMap<Long, Worker> workers = new HashMap<>();
    private static HashMap<String, Long> idNameHashMap = new HashMap<>();


    @Override
    public List<Worker> findAllWorkers() {
        // Pagination should be added...
        return jdbcTemplate.query(
                "SELECT * FROM workers",
                (rs, rowNum) ->
                        new Worker(
                                rs.getInt("id"),
                                rs.getString("name"),
                                rs.getDouble("tunjangan_pulsa"),
                                rs.getInt("gaji_pokok"),
                                rs.getInt("absen"),
                                rs.getString("worker_type")

                        )
        );

    }


    @Override
    public Worker findById(int id)  {
        return jdbcTemplate.queryForObject(
                "select * from workers where id = ?",
                new Object[]{id},
                (rs, rowNum) ->
                        new Worker(
                                rs.getInt("id"),
                                rs.getString("name"),
                                rs.getDouble("tunjangan_pulsa"),
                                rs.getInt("gaji_pokok"),
                                rs.getInt("absen"),
                                rs.getString("worker_type")
                        )
        );
    }

    @Override
    public Worker findByName(String name) {
        if (idNameHashMap.get(name) != null) {
            return workers.get(idNameHashMap.get(name));
        }

        return null;
    }


    @Override
    public void saveWorker(Worker worker)  {
        jdbcTemplate.update(
                "insert into workers (name, worker_type, gaji_pokok, tunjangan_pulsa, absen ) values(?, ?, ?, ?, ?)",
                worker.getName(), worker.getWorkerType(), worker.getGajiPokok(), worker.getTunjanganPulsa(), worker.getAbsen());
    }

    @Override
    public void updateWorker(Worker worker)  {
        jdbcTemplate.update(
                "UPDATE workers SET name = ?,  worker_type = ?, gaji_pokok = ?, tunjangan_pulsa = ?  WHERE id = ?",
                worker.getName(), worker.getWorkerType(), worker.getGajiPokok(), worker.getTunjanganPulsa(), worker.getId()
        );
    }

    @Override
    public void addAbsen(Worker worker) {
        jdbcTemplate.update(
                "UPDATE workers SET absen = ?  WHERE id = ?",
                (worker.getAbsen()), worker.getId()

        );
    }

    @Override
    public void deleteWorkerById(int id)  {

    }


    public boolean isWorkerExist(Worker worker) {
        return findByName(worker.getName()) != null;
    }

    public void deleteAllProducts() {workers.clear();
    }

}

