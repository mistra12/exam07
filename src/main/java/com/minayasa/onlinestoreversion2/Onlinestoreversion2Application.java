package com.minayasa.onlinestoreversion2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={"com.minayasa.onlinestoreversion2"})
public class Onlinestoreversion2Application {

	public static void main(String[] args) {
		SpringApplication.run(Onlinestoreversion2Application.class, args);
	}

}
